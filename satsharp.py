"""
FILE: satsharp.py
VER: 2.2 
AUTHOR: Juan Manuel Dato
DESC: Version in English to recognize SAT formulas
to count the number of cases, using the operators
of Python for the construction of the entry.
From 2.0 I repair the bugs and I eliminated capability to make observations, it was in a wrong.
"""
 
class Literal:
    def __init__(self, astring):
        if astring[0]=='-':
            self.body=astring[1:]
            self.negative=True
        else:
            self.body=astring
            self.negative=False

    def __xor__(self, aBoolean):
        R=Literal(self.body)
        R.negative=self.negative^aBoolean
        return R
    
    def __neg__(self):
        R=Literal(self.body)
        R.negative=not self.negative
        return R

    def __repr__(self):
        return ('-' if self.negative else '')+self.body

class TreeDerivation4:
    'Literals converted in integers'
    #A=B*C ---> A:(B,C) with A positive, B and C literals
    def __init__(self, objective, assignments={}):
        names={objective.body:2} #for each literal a number is allocated
        self.concept=-2 if objective.negative else 2
        negate=[]
        self.assignments={}
        negative=lambda astring:astring[0]=='-'
        body= lambda astring: astring if not negative(astring) else astring[1:]
        for (cA,(B,C)) in assignments.items():
            if body(cA) not in names.keys():
                names[body(cA)]=len(names)+2
            if negative(cA):
                negate.append(names[body(cA)])
            if B.body not in names.keys():
                names[B.body]=len(names)+2
            if C.body not in names.keys():
                names[C.body]=len(names)+2
            self.assignments[names[body(cA)]]=(
                names[B.body]*(-1 if B.negative else 1),
                names[C.body]*(-1 if C.negative else 1))
        if negate:
            if self.concept in negate:
                self.concept=-self.concept
            for (iA,(iB,iC)) in self.assignments.items():
                if abs(iB) in negate:
                    iB=-iB
                if abs(iC) in negate:
                    iC=-iC
                self.assignments[iA]=(iB,iC)
    def __repr__(self):
        return repr((self.concept,self.assignments))
    
    
"""
Dado un tree NEG,AND,OR,IMP (tree NAOI) convertirlo en raiz y assignments
"""

class TreeNAOI:
    (NEG,AND,OR,IMP)=range(4)
    op="-&|>"
    
    def __init__(self, tree):
        'Lo más primitivo son Literal'
        if type(tree) is str:
            self.body=Literal(tree)
        else:
            self.body=tree
        if type(self.body) is Literal:
            if self.body.negative:
                self.body=(TreeNAOI.NEG,
                             TreeNAOI(Literal(self.body.body)))
        
    def __repr__(self):
        if type(self.body) is str:
            return self.body
        elif type(self.body) is Literal:
            return str(self.body)
        elif self.body[0]==self.NEG:
            return "("+self.op[self.NEG]+", "+repr(self.body[1])+")"
        else:
            return "("+self.op[self.body[0]]+", "+ \
                          repr(self.body[1])+", "+\
                          repr(self.body[2])+")"

    def __and__(self,other):
        return TreeNAOI((TreeNAOI.AND, self, other))

    def __or__(self, other):
        return TreeNAOI((TreeNAOI.OR, self, other))

    def __neg__(self):
        return TreeNAOI((TreeNAOI.NEG, self))

    def __gt__(self,other):
        'self>other implication: if self then other'
        return TreeNAOI((TreeNAOI.IMP, self,other))

    def literals(self):
        if type(self.body) is Literal:
            return set([self.body.body])
        elif self.body[0]==TreeNAOI.NEG:
            return self.body[1].literals()
        else:
            R=self.body[1].literals()
            return R.union(self.body[2].literals())

    def literal(self):
        if type(self.body) is Literal:
            return self.body.body
        elif self.body[0]==TreeNAOI.NEG:
            if type(self.body[1].body) is Literal:
                return self.op[TreeNAOI.NEG]+self.body[1].body.body
            elif self.body[1].body[0]==TreeNAOI.NEG:
                return self.body[1].body[1].literal()
            else:
                return False
        else:
            return False

    def toAssignments(self, auxiliars=0, prefix='z'):
        L=self.literal()
        if L:
            return Literal(L),{},auxiliars
        A={}
        if self.body[0]==self.NEG:
            V,A,auxiliars= self.body[1].toAssignments(auxiliars,prefix)
            return -V,A,auxiliars
        elif self.body[0]==self.AND:
            variable=prefix+repr(auxiliars)
            arg1,newAssig,auxiliars=self.body[1].toAssignments(
                auxiliars+1,prefix)
            A.update(newAssig)
            arg2,newAssig,auxiliars=self.body[2].toAssignments(
                auxiliars,prefix)
            A.update(newAssig)
            A[variable]=(arg1,arg2)
            return Literal(variable),A,auxiliars
        elif self.body[0]==self.OR:
            variable=self.op[TreeNAOI.NEG]+prefix+repr(auxiliars)
            arg1,newAssig,auxiliars=self.body[1].toAssignments(
                auxiliars+1,prefix)
            A.update(newAssig)
            arg2,newAssig,auxiliars=self.body[2].toAssignments(
                auxiliars,prefix)
            A.update(newAssig)
            A[variable]=(-arg1,-arg2)
            return -Literal(variable),A,auxiliars
        elif self.body[0]==self.IMP:
            variable=self.op[TreeNAOI.NEG]+prefix+repr(auxiliars)
            arg1,newAssig,auxiliars=self.body[1].toAssignments(
                auxiliars+1,prefix)
            A.update(newAssig)
            arg2,newAssig,auxiliars=self.body[2].toAssignments(
                auxiliars,prefix)
            A.update(newAssig)
            A[variable]=(arg1,-arg2)
            return -Literal(variable),A,auxiliars
        else:
            raise Exception
    @staticmethod
    def literalsN(variables, prefix='v',ini=1):
        return [TreeNAOI(Literal(prefix+repr(X))) \
                for X in range(ini,variables+ini)]           


# > sharp(test1()) ==> 4
        
def test1():
    #Theorem
    P=TreeNAOI(Literal("v1"))
    Q=TreeNAOI(Literal("v2"))
    return (P&(P>Q))>Q

def test2():
    #Theorem
    P=TreeNAOI(Literal("v1"))
    Q=TreeNAOI(Literal("v2"))
    return (P>(Q>P))

def test3():
    #Not Theorem
    P=TreeNAOI(Literal("v1"))
    Q=TreeNAOI(Literal("v2"))
    R=TreeNAOI(Literal("v3"))
    return ((P>R)>(P>Q))>(R>Q)

def test4():
    P=TreeNAOI(Literal("v1"))
    Q=TreeNAOI(Literal("v2"))
    return P&Q

def test5():
    P=TreeNAOI(Literal("v1"))
    Q=TreeNAOI(Literal("v2"))
    return P|Q

def test6():
    P=TreeNAOI(Literal("v1"))
    Q=TreeNAOI(Literal("v2"))
    return P>Q

def test7():
    #Theorem
    P=TreeNAOI(Literal("v1"))
    Q=TreeNAOI(Literal("v2"))
    R=TreeNAOI(Literal("v3"))
    return ((P>(Q>R))>((P>Q)>(P>R)))
    
from csharp import _scales, spectrum, Scale
def sharp(aTreeNAOI):
    objective,A,aux=aTreeNAOI.toAssignments()
    NN,assig,semAt=createWorkSpace(A, objective.body)
    ultPair=(3*len(NN)+1)*2+12*len(A)
    semAt[objective.body]=ultPair-1
    assig[ultPair]=0 #objective is opposite to ultPair
    assig,semAt=createPairs(A,(3*len(NN)+1)*2,assig,semAt)
    items=[X for X in assig.items()]
    items.sort()
    return spectrum(*[Scale(*X) \
        for X in _scales(items, ultPair)])
      
def nAsigNonnegativ(A):
    NN=set()
    for p,q in A.values():
        if not p.negative:
            NN.add(p.body)
        if not q.negative:
            NN.add(q.body)
    for k in A.keys():
        if k[0]=='-':
            NN.add(k[1:])
    return list(NN)

def createWorkSpace(A, obj):
    NN=nAsigNonnegativ(A)
    if obj in NN:
        NN.remove(obj)
    assig={0:0}
    semAt={}
    P=2
    for X in NN:
        assig[P+3]=1
        semAt[X]=P
        P+=6
    return NN,assig,semAt

def createPairs(A, P, assig, semAt):
    for cA,(B,C) in A.items():
        assig[P]=P
        assig[P+6]=P #logic bridge
        assig[P+9]=1 #separator

        if not B.body in semAt.keys():
            semAt[B.body]=P+1
        else:
            X=semAt[B.body]+int(not B.negative)
            if not X in assig.keys():
                assig[X]=X
            assig[P+1]=assig[X]
            
        if not C.body in semAt.keys():
            semAt[C.body]=P+3
        else:
            X=semAt[C.body]+int(not C.negative)
            if not X in assig.keys():
                assig[X]=X
            assig[P+3]=assig[X]
            
        if not Literal(cA).body in semAt.keys():
            semAt[Literal(cA).body]=P+2
        else:
            X=semAt[Literal(cA).body]+\
               int(Literal(cA).negative)
            if not X in assig.keys():
                assig[X]=X
            assig[P+2]=assig[X]
        P+=12
    return assig, semAt

      


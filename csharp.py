"""
FILE: csharp.py
VER: 3.0 In English
AUTHOR: Juan Manuel Dato
EMAIL: jumadaru@gmail.com
DESC 
"""
def fib(n):
    if n<2:
        return 1
    a=1
    b=1
    for i in range(2,n+1):
        a,b = a+b, a
    return a

def sequences(alt3, ultAux, assignments={}):
    'sequences from choices of 3 literals'
    assignments={}
    R=[alt3[0]]
    for A in alt3[1:]:
        if not R[-1][2]==A[0]:
            R.append([A[-1][2],ultAux+1,ultAux+2])
            R.append([ultAux+2,ultAux+3,ultAux+4])
            R.append([ultAux+4,ultAux+5,A[0]])
            assignments[ultAux+3]=1
        R.append(A)
    return R,assignments,ultAux    

def pairs(sequences, assignments):
    'sequences from assignments'
    P=[assignments[sequences[0][0]]] \
       if sequences[0][0] in assignments.keys() else []
    proxAux=2
    closed={sequences[0][0]:None}
    for [A,B,C] in sequences:
        if B in assignments.keys():
            P.append((B,assignments[B]))
        if B in closed.keys():
            if closed[B] is None:
                closed[B]=proxAux
                proxAux+=1
            P.append((B,closed[B]))
        if C in assignments.keys():
            P.append((C,assignments[C]))
        if C in closed.keys():
            if closed[C] is None:
                closed[C]=proxAux
                proxAux+=1
            P.append((C,closed[C]))
    return P, proxAux       

def _scales(pairs, ultPar):
    'pairs is list of bridge and its descriptor'
    if not pairs:
        return [(ultPar//2,None,None,None,None)]
    if not pairs[0][0]==0:
        pairs.insert(0,(0,None))
    if not pairs[-1][0]==ultPar:
        pairs.append((ultPar,None))
    R=[]
    pairIni=0
    pairEnd=None
    r,t,n,h=None,None,None,None
    for pos, valor in pairs:
        if pairEnd is None:
            if pos%2==0 and pos>pairIni:
                R.append(((pos-pairIni)//2,r,t,n,valor))
                r,t,n,h=valor,None,None,None
                pairIni=pos
                pairEnd=None
            elif pos%2==0:
                r=valor
            elif pos>pairIni+1:
                n=valor
                pairEnd=pos+1
            else:
                t=valor
        else:
            if pos==pairEnd:
                h=valor
                R.append(((pairEnd-pairIni)//2,r,t,n,h))
                r,t,n,h=h,None,None,None
                pairIni=pos
                pairEnd=None
            else:
                R.append(((pairEnd-pairIni)//2,r,t,n,h))
                if pos%2==0:
                    R.append(((pos-pairEnd)//2,h,None,None,valor))
                    r,t,n,h=valor,None,None,None
                    pairIni=pos
                    pairEnd=None
                elif pos==pairEnd+1:
                    pairIni=pairEnd
                    pairEnd=None
                    r,t,n,h=h,valor,None,None
                else:
                    pairIni=pairEnd
                    pairEnd=pos+1
                    r,t,n,h=h,None,valor,None
    return R
           

def spectrum(*scales):
    W=[scales[0](k) for k in range(16)]
    for Sk in scales[1:]:
        
        V=[Context() for k in range(16)]
        for i in range(4):
            for x in range(16):
                V[4*i+x%4]+=W[4*i+int(x>=8)]*Sk(x)+\
                         W[4*i+int(x>=8)+2]*Sk(x)
        for k in range(16):
            W[k]=V[k]
    Sum=Context()
    for X in W:
        Sum+=X
    return Sum[None]

class Context:
    def __init__(self, cardinal=0, columns=[0], tuples=[], Dic={}):
        'Column 0 is for the cardinal'
        if not Dic:
            self.cardinal=cardinal
            self.columns=columns if columns[0]==0 else [0]+columns
            self.tuples=tuples
        else:
            self.cardinal=Dic[None] if None in Dic.keys() else cardinal
            self.columns=[0]+[X for X in Dic.keys() if not X is None]
            self.tuples=[[self.cardinal]+[Dic[X] for X in Dic.keys()\
                                          if not X is None]]
    def __repr__(self):
        #self.m() #DEBUG MODE
        return "Context"+repr((self.cardinal,self.columns,self.tuples))
    def clone(self):
        return Context(self.cardinal,
                       self.columns[:],
                       [t[:] for t in self.tuples])
    def __bool__(self):
        return self.cardinal>0
    @staticmethod
    def comp(X,Y):
        'If Y is compatible con X'
        return X is None or Y is None or X==Y
    @staticmethod
    def AND(X,Y):
        if X is None:
            return Y
        elif Y is None or X==Y:
            return X
        else:
            return 2
    def __getuniqueitem(self, column):
        'called by __getitem__'
        P=self.columns.index(abs(column))
        S=0
        for atuple in self.tuples:
            if Context.comp(atuple[P],int(column>0)):
                S+=atuple[0]
        return S
    def __getitem__(self, columns):
        if columns is None:
            return self.cardinal
        if type(columns) is int:
            return self.__getuniqueitem(columns)
        P=[self.columns.index(abs(c)) for c in columns]
        S=0
        for atuple in self.tuples:
            incompatible=False
            for i in range(len(columns)):
                if not Context.comp(atuple[P[i]],int(columns[i]>0)):
                    incompatible=True
            if not incompatible:
                S+=atuple[0]
        return S
    def _filter(self, columns, atuple):
        result=self.clone()
        for C in range(len(columns)):
            if not columns[C] in self.columns:
                result.columns.append(columns[C])
                result.tuples=[t+[atuple[C]] for t in result.tuples]

        result.cardinal=0
        for t in range(len(self.tuples)):
            for i in range(1,len(self.columns)):
                if self.columns[i] in columns:
                    result.tuples[t][i]=Context.AND(
                        self.tuples[t][i],
                        atuple[columns.index(self.columns[i])])
                    if result.tuples[t][i]==2:
                        result.tuples[t][0]=0
            result.tuples[t][0]*=atuple[0]
            result.cardinal+=result.tuples[t][0]

        result.tuples=[t for t in result.tuples if t[0]>0]
        return result
    def __mul__(self, other):
        if not self or not other:
            return Context()
        R=Context()
        for atuple in other.tuples:
            R+=self._filter(other.columns,atuple)
        return R
    def __add__(self,other):
        if not self:
            return other
        if not other:
            return self
        R=self.clone()
        for atuple in other.tuples:
            R=R.overlay(other.columns,atuple)
        return R
    def insert(self,*atuple):
        'Must be incompatible with the rest'
        self.tuples.append(list(atuple))
        self.cardinal+=atuple[0]
    @staticmethod
    def cont(tX,tY):
        'If all cases of tY in tX'
        for j in range(len(tX)):
            if not (tX[j] is None or tX[j]==tY[j]):
                return False
        return True
    @staticmethod
    def adjust(col1,tup1,col2,tup2):
        'If all cases of tup2 in tup1'
        for p2 in range(1,len(col2)):
            if not col2[p2] in col1:
                return False
            p1=col1.index(col2[p2])
            if not (tup1[p1] is None or tup1[p1]==tup2[p2]):
                return False
        for p1 in range(1,len(col1)):
            if not col1[p1] in col2:
                if not tup1[p1] is None:
                    return False
        return True
                
    def overlay(self, columns, atuple):
        result=self.clone()
        for t in result.tuples:
            if Context.adjust(result.columns, t, columns, atuple):
                t[0]+=atuple[0]
                result.cardinal+=atuple[0]
                return result
        newTuple=[None]*len(result.columns)
        for p in range(len(columns)):
            if not columns[p] in result.columns:
                result.columns.append(columns[p])
                result.tuples=[t+[None] for t in result.tuples]
                newTuple.append(atuple[p])
            else:
                newTuple[result.columns.index(columns[p])]=atuple[p]
        result.insert(*newTuple)
        return result
        
    def m(self):
        print(*self.columns,sep='\t')
        for t in self.tuples:
            print(*t,sep='\t')
        print(self.cardinal)

class Scale:
    def __init__(self, l, r=None,t=None,n=None,h=None):
        self.r=r #rattle
        self.t=t #tail
        self.n=n #neck
        self.h=h #head
        self.l=l #length

    def __call__(self, X):
        'Número del 0 al 15'
        R=int(X>=8)
        X%=8
        T=int(X>=4)
        X%=4
        N=int(X>=2)
        H=X%2
        thecontext={}
        for a,A in (self.r,R),(self.t,T),(self.n,N),(self.h,H):            
            if not a is None:
                if a>1:
                    if a in thecontext.keys():
                        if not thecontext[a]==A:
                            return Context()
                    else:
                        thecontext[a]=A
                elif not a==A:
                    return Context()
        thecontext[None]=S(self.l,R,T,N,H)
        if thecontext[None]==0:
            return Context()
        return Context(Dic=thecontext)


def pc1():
    C=Context()
    C.columns=[0,2,3,4]
    C.insert(1,1,None,0)
    C.insert(3,0,1,None)
    return C

def ps3():
    #3
    P=(Scale(2,2,None,2,3),Scale(1,3,None,None,2))
    return spectrum(*P)

def ps4(a,b):
    P=(Scale(2,2,a,b,3),Scale(1,3,None,None,2))
    return spectrum(*P)

def ps1():
    #4
    P=(Scale(2,0,None,4,10),
       Scale(3,10,1,3,11),
       Scale(2,11,1,None,6),
       Scale(2,6,8,1,12),
       Scale(1,12,None,None,2),
       Scale(1,2,3,None,4),
       Scale(2,4,9,None,2),
       Scale(2,2,None,1,13),
       Scale(1,13,None,None,5),
       Scale(1,5,4,None,6),
       Scale(2,6,3,None,5),
       Scale(2,5,None,1,14),
       Scale(1,14,None,None,7),
       Scale(1,7,8,None,0),
       Scale(2,0,9,None,7))
    return spectrum(*P)

def ps11():
    #4
    P=(Scale(2,0,None,4,None),
       Scale(3,None,1,3,None),
       Scale(2,None,1,None,6),
       Scale(2,6,8,1,None),
       Scale(1,None,None,None,2),
       Scale(1,2,3,None,4),
       Scale(2,4,9,None,2),
       Scale(2,2,None,1,None),
       Scale(1,None,None,None,5),
       Scale(1,5,4,None,6),
       Scale(2,6,3,None,5),
       Scale(2,5,None,1,None),
       Scale(1,None,None,None,7),
       Scale(1,7,8,None,0),
       Scale(2,0,9,None,7))
    return spectrum(*P)


def ps2():
    #7
    P=(Scale(1,5,None,None,2),
       Scale(2,2,3,4,None),
       Scale(2,None,2,4,3),
       Scale(2,3,None,None,5))
    return spectrum(*P)




def S(lon, rattle=None,tail=None,neck=None,head=None):
    if lon==1: #tail==neck (rattle|tail|head)=1
        if tail is None and not neck is None:
            tail=neck
        elif not tail is None and neck is None:
            neck=tail
        elif not tail==neck:
            return 0
        if tail==1 and (head==1 or rattle==1):
            return 0
        elif tail==0 and head==0 and rattle==0:
            return 0
        elif head==1 and rattle==1:
            return 0
        if head==1 or neck==1 or rattle==1:
            return 1
        return int(head is None)+int(neck is None)+int(rattle is None)
    else:
        if head==1:
            return S(lon-1,head=rattle, neck=tail, rattle=0) \
                   if not neck==1 else 0
        elif head==0:
            return S(lon-1,head=None if neck is None else 1-neck,
                     tail=tail, rattle=rattle)
        elif not neck is None:
            return S(lon,rattle=rattle,tail=tail,head=neck)
        elif rattle is None and tail is None:
            return fib(lon+2)
        else:
            return S(lon,head=rattle, neck=tail)

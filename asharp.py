"""
FILE: asharp.py
VER: 2.2
AUTHOR: Juan Manuel Dato
DESC Mechanism based in rules of the form A = B & C where A is positive
"""
#In this document I do not use the cardinal of the Context
from csharp import Context

"""
Cálculo analítico del sharp
Dado A = B & C
#(-A)=#(-B)+#(-C)-#(-B,-C)
#(A)=#(B,C)
"""

memo={}
def _sharp(rules, obj):
    global memo
    if not abs(obj) in rules.keys():
        return Context(1,Dic={abs(obj):int(obj>0)}),Context()
    if obj>0:
        if not abs(rules[obj][0]) in rules.keys():
            R1=(Context(1,Dic={abs(rules[obj][0]):int(rules[obj][0]>0)}),Context())
        else:
            if not rules[obj][0] in memo:
                memo[rules[obj][0]]=_sharp(rules,rules[obj][0])
            R1=memo[rules[obj][0]]
        if not abs(rules[obj][1]) in rules.keys():
            R2=(Context(1,Dic={abs(rules[obj][1]):int(rules[obj][1]>0)}),Context())
        else:    
            if not rules[obj][1] in memo:
                memo[rules[obj][1]]=_sharp(rules,rules[obj][1]) 
            R2=memo[rules[obj][1]]
        return R1[0]*R2[0]+R1[1]*R2[1],R1[0]*R2[1]+R1[1]*R2[0]
    else:
        if not abs(rules[-obj][0]) in rules.keys():
            R1=(Context(1,Dic={abs(rules[-obj][0]):int(rules[-obj][0]<0)}),Context())
        else:
            if not rules[-obj][0] in memo:
                memo[rules[-obj][0]]=_sharp(rules,-rules[-obj][0])
            R1 = memo[rules[-obj][0]]
        if not abs(rules[-obj][1]) in rules.keys():
            R2=(Context(1,Dic={abs(rules[-obj][1]):int(rules[-obj][1]<0)}),Context())
        else:
            if not rules[-obj][1] in memo:
                memo[rules[-obj][1]]=_sharp(rules,-rules[-obj][1])
            R2 = memo[rules[-obj][1]]
        return (R1[0]+R2[0]+R1[0]*R2[1]+R1[1]*R2[0],
                R1[1]+R2[1]+R1[0]*R2[0]+R1[1]*R2[1])
def sharp(rules, obj):
    S,R=_sharp(rules,obj)
    casos=0
    for atuple in S.tuples:
        casos+=atuple[0]*2**len([X for X in atuple[1:] if X is None])
    for atuple in R.tuples:
        casos-=atuple[0]*2**len([X for X in atuple[1:] if X is None])
    return casos 
def test1():
    global memo
    memo={}
    return sharp({4:(2,-3),5:(-4,2),6:(5,-3)},-6)

def test2():
    'A>(B>A)'
    global memo
    memo={}
    AD={3:(2,-1),4:(1,3)}
    return sharp(AD,-4)

def test3():
    global memo
    memo={} 
    T={4:(1,-2),
       5:(1,-3),
       6:(2,-3),
       7:(-4,5),
       8:(-7,6)}
    return sharp(T,-8)

